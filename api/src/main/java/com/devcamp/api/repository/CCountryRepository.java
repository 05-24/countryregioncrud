package com.devcamp.api.repository;

import com.devcamp.api.model.CCountry;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CCountryRepository extends JpaRepository<CCountry,Long>{
    
}
