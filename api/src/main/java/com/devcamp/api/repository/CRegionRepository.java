package com.devcamp.api.repository;

import com.devcamp.api.model.CRegion;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CRegionRepository extends JpaRepository<CRegion,Long>{
    
}
