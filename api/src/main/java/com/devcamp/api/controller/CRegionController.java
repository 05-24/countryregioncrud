package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.api.model.CCountry;
import com.devcamp.api.model.CRegion;
import com.devcamp.api.repository.CCountryRepository;
import com.devcamp.api.repository.CRegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.StackWalker.Option;
import java.util.ArrayList;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CRegionController {
    
    @Autowired
    CCountryRepository countryRepository;

    @Autowired
    CRegionRepository regionRepository;

    //Lấy tất cả danh sách region
    @GetMapping("/regions")
    public ResponseEntity<List<CRegion>> getAllRegions(){
        try {
            List<CRegion> allRegions = new ArrayList<>();
            regionRepository.findAll().forEach(allRegions::add);
            return new ResponseEntity<>(allRegions,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy một region theo id 
    @GetMapping("/regions/{id}")
    public ResponseEntity<CRegion> getRegionById(@PathVariable("id") long id){
        try {
            Optional<CRegion> findRegion = regionRepository.findById(id);
            if(findRegion.isPresent()){
                CRegion regionData = findRegion.get();
                return new ResponseEntity<>(regionData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy tất cả region của country theo id 
    @GetMapping("/countries/{id}/regions")
    public ResponseEntity<List<CRegion>> getAllRegionByCountryId(@PathVariable("id") long id){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                CCountry countryData = findCountry.get();
                List<CRegion> regionsOfCountry = countryData.getRegions();
                return new ResponseEntity<>(regionsOfCountry,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả danh sách region
    @DeleteMapping("/regions")
    public ResponseEntity<CRegion> deleteAllRegions(){
        try {
            regionRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá region theo id
    @DeleteMapping("/regions/{id}")
    public ResponseEntity<CRegion> deleteRegionById(@PathVariable("id") long id){
        try {
            Optional<CRegion> findRegion = regionRepository.findById(id);
            if(findRegion.isPresent()){
                regionRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới một region vào một country theo country id
    @PostMapping("/countries/{id}/regions")
    public ResponseEntity<Object> createRegion(@PathVariable("id") long id,@Valid @RequestBody CRegion pRegion){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                CRegion newRegion = new CRegion();
                CCountry countryData = findCountry.get();
                newRegion.setRegionCode(pRegion.getRegionCode());
                newRegion.setRegionName(pRegion.getRegionName());
                newRegion.setCountry(countryData);

                CRegion saveRegion = regionRepository.save(newRegion);
                return new ResponseEntity<>(saveRegion,HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Update một region theo region id
    @PutMapping("/regions/{id}")
    public ResponseEntity<Object> updateRegion(@PathVariable("id") long id,@Valid @RequestBody CRegion pRegion){
        try {
            Optional<CRegion> findRegion = regionRepository.findById(id);
            if(findRegion.isPresent()){
                CRegion regionData = findRegion.get();
                regionData.setRegionCode(pRegion.getRegionName());
                regionData.setRegionName(pRegion.getRegionName());

                CRegion saveRegion = regionRepository.save(regionData);
                return new ResponseEntity<>(regionData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm tất cả region
    @GetMapping("/regions/count")
    public ResponseEntity<Long> countRegion(){
        try {
            Long count = regionRepository.count();
            return new ResponseEntity<Long>(count,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm tất cả regions trong một country theo id
    @GetMapping("/countries/{id}/regions/count")
    public ResponseEntity<Integer> countRegionOfCountry(@PathVariable("id") long id){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                CCountry countryData = findCountry.get();
                List<CRegion> allRegionsOfCountry = countryData.getRegions();
                Integer count = allRegionsOfCountry.size();
                return new ResponseEntity<>(count,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm kiểm tra region có id có tồn tại không
    @GetMapping("/regions/{id}/check")
    public ResponseEntity<Boolean> checkRegion(@PathVariable("id") long id){
        try {
            Optional<CRegion> findRegion = regionRepository.findById(id);
            if(findRegion.isPresent()){
                return new ResponseEntity<>(true,HttpStatus.FOUND);
            } else {
                return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
