package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.api.model.CCountry;
import com.devcamp.api.repository.CCountryRepository;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.StackWalker.Option;
import java.util.ArrayList;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CCountryController {
    
    @Autowired
    CCountryRepository countryRepository;

    //Lấy tất cả danh sách country
    @GetMapping("/countries")
    public ResponseEntity<List<CCountry>> getAllCountries(){
        try {
            List<CCountry> allCountries = new ArrayList<>();
            countryRepository.findAll().forEach(allCountries::add);
            return new ResponseEntity<>(allCountries,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy một country bởi country id 
    @GetMapping("/countries/{id}")
    public ResponseEntity<CCountry> getCountryById(@PathVariable("id") long id){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                CCountry countryData = findCountry.get();
                return new ResponseEntity<CCountry>(countryData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả country 
    @DeleteMapping("/countries")
    public ResponseEntity<CCountry> deleteAllCountries(){
        try {
            countryRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá một country theo id
    @DeleteMapping("countries/{id}")
    public ResponseEntity<CCountry> deleteCountryById(@PathVariable("id") long id){
        try {
            countryRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }  catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới một country 
    @PostMapping("/countries")
    public ResponseEntity<Object> createCountry(@Valid @RequestBody CCountry pCountry){
        try {
            CCountry newCountry = new CCountry();
            newCountry.setCountryCode(pCountry.getCountryCode());
            newCountry.setCountryName(pCountry.getCountryName());
            newCountry.setRegions(pCountry.getRegions());

            CCountry saveCountry = countryRepository.save(newCountry);
            return new ResponseEntity<>(saveCountry,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Update một country 
    @PutMapping("/countries/{id}")
    public ResponseEntity<Object> updateCountry(@PathVariable("id") long id,@Valid @RequestBody CCountry pCountry){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                CCountry countryData = findCountry.get();
                countryData.setCountryCode(pCountry.getCountryCode());
                countryData.setCountryName(pCountry.getCountryName());
                countryData.setRegions(pCountry.getRegions());

                CCountry saveCountry = countryRepository.save(countryData);
                return new ResponseEntity<>(saveCountry,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm số country
    @GetMapping("/countries/count")
    public ResponseEntity<Long> countCountry(){
        try {
            Long count = countryRepository.count();
            return new ResponseEntity<Long>(count,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //Hàm kiểm tra xem country có id có tồn tại không ?
    @GetMapping("/countries/{id}/check")
    public ResponseEntity<Boolean> checkCountry(@PathVariable("id") long id){
        try {
            Optional<CCountry> findCountry = countryRepository.findById(id);
            if(findCountry.isPresent()){
                return new ResponseEntity<>(true,HttpStatus.FOUND);
            } else {
                return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
